---
cascade:
  featured_image: /images/unstableunicorn-hacking.png
description: My reflections on Agile, DevOps, Transformation, Architecture, Testing,
  Programming or maybe just a book I'm reading...
title: Some Unstable Thoughts
---
Welcome to my blog, it's a work in progress and I hope to have content up here soon!
